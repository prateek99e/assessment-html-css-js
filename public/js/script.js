let productsitems;
let saveforlater = [];


const getProducts = () => {
 return fetch("http://localhost:3000/products")
    .then((result) => {
    if (result.status == 200) {
     return Promise.resolve(result.json());
     } else {
    
return Promise.reject("Unable to retrieve the product list");
     }
}).then(resultProducts => {
    productsitems = resultProducts;
     //Populate into the DOM
    createProductList();
     return productsitems;
     }).catch(error => { 
throw new Error(error);
   
    }) 
   }

//Get the Saveforlater list
function getSaveForLater() {
 //API call
 return fetch("http://localhost:3000/saveforlater").then((result) => {
  
 if (result.status == 200) {
 
 return Promise.resolve(result.json());
   
    
 } else {
     
 return Promise.reject("Error");
   
}
   
 }).then(result => {
         saveforlater = result;
   createSaveForLaterList(); 
 return result;
   
 }).catch(error => {  
 throw new Error(error);  
 })
    }


function addProduct(id) {

 if (!isProductPresentInSaveForLater(id)) {
 let productObject = getProductById(id)
 saveforlater.push(productObject);

 //Add product call
return fetch("http://localhost:3000/saveforlater", {
     method: 'POST',
 body: JSON.stringify(productObject),
 headers: {
'Content-Type': 'application/json',
 'Accept': 'application/json'
 }


 }).then((result) => {

if (result.status == 200 || result.status == 201) {

return Promise.resolve(saveforlater);
 } else {
  return Promise.reject("Product is already saved for Later")
    }

 }).then((saveforlaterResult) => {

createFavouriteList();

 return saveforlaterResult;
})
 } else {

alert("Product is already saved for later");

 }
}
function isProductPresentInSaveForLater(selectedProductId) {
 for (let item in saveforlater) {

 if (selectedProductId == saveforlater[item].id) {
return true;}

}

 return false;

}

function getProductById(id) {
 for (let product in productsitems) {

 if (id == productsitems[product].id) {
 return productsitems[product];

 }

 }

}

const createProductList = () => {

 let domProductList = '';
 productsitems.forEach(element => {
 domProductList = domProductList + `<div class="card" style="width:400px">
<img class="img-fluid " src="${element.thumbnail}" alt="Card image">
<div class="card-body">
<h4 class="card-title" >${element.title} </h4>
<p class="Description">${element.description}</p>

<button
onclick="addProduct(${element.id})" type="button" class="btn btn-primary">
SaveForLater
</button><br></div>
</div><br>`;
});

 document.getElementById("products").innerHTML = domProductList;
}


const createSaveForLaterList = () => {
 let domSaveForLaterList = '';

 let childNode = document.getElementById("saveforlater");
 childNode.innerHTML = '';

 saveforlater.forEach(element => {
 domSaveForLaterList = domSaveForLaterList + `<div class="card" style="width:400px">
<img class="img-fluid " src="${element.thumbnail}" alt="Card image">
<div class="card-body">
<h4 class="card-title">${element.title}</h4>
<p class="Description">${element.description}</p>

<button
onclick="removeProduct(${element.id})" type="button" class="btn btn-danger">
Remove from SaveForLater
</button><br></div>
</div><br>`;
 });

 childNode.innerHTML = domSaveForLaterList;
}

function removeProduct(id) {

 saveforlater = saveforlater.filter(item => item.id !== id);
fetch('http://localhost:3000/saveforLater/' + id, { method: 'DELETE',
 })
 .then((result) => {

 if (result.status == 200) {

 createSaveForLaterList();

alert("Deleted");
} else {
 throw new Error("unable to remove")

 }
 })

.catch(error => {

 throw new Error(error);
 });
}

getProducts();
getSaveForLater();